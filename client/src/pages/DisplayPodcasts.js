import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import  PodcastCard  from '../components/PodcastCard';
import { CircularProgress } from '@mui/material';

const DisplayMain = styled.div`
display: flex;
padding: 30px 30px;
flex-direction: column;
height: 100%;
overflow-y: scroll;
`
const Topic = styled.div`
  color: ${({ theme }) => theme.text_primary};
  font-size: 24px;
  font-weight: 540;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const Podcasts = styled.div`
display: flex;
flex-wrap: wrap;
height: 100%;
gap: 10px;
padding: 30px 0px;
`
const Container = styled.div`
background-color: ${({ theme }) => theme.bg};
padding: 20px;
border-radius: 6px;
min-height: 400px;
`

const Loader = styled.div`
display: flex;
justify-content: center;
align-items: center;
height: 100%;
width: 100%;
`
const DisplayNo = styled.div`
display: flex;
justify-content: center;
align-items: center;
height: 100%;
width: 100%;
color: ${({ theme }) => theme.text_primary};
`




const DisplayPodcasts = () => {
    const { gener } = useParams();
    const [podcasts, setPodcasts] = useState([1,2,3,4,5]);
    const [Loading, setLoading] = useState(false);

    useEffect(() => {
        fetch(`http://127.0.0.1:2900/podbygener/${gener}`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setPodcasts(data);
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
    }, [gener]);
    
    return (
        <DisplayMain>
            <Container>
                <Topic>{gener.replace(/-/g, ' ').toUpperCase()}</Topic>
                {Loading ? 
                <Loader>
                    <CircularProgress />
                </Loader>
                 :
                    <Podcasts>
                        {podcasts.length === 0 && <DisplayNo>No Podcasts</DisplayNo>}
                        {podcasts.map((item,index) => (
                            <PodcastCard item={item} key={index} />
                        ))}
                    </Podcasts>
                }
            </Container>
        </DisplayMain>
    )
}

export default DisplayPodcasts