import React, { useEffect } from 'react'
import PodcastCard from '../components/PodcastCard'
import styled from 'styled-components';
import { useFavorite } from '../components/FavoriteContext';
// import { useAuth } from '../components/AuthContext';
import { useNavigate } from 'react-router-dom';

const Container = styled.div`
padding: 20px 30px;
padding-bottom: 200px;
height: 100%;
overflow-y: scroll;
display: flex;
flex-direction: column;
gap: 20px;
`
const Topic = styled.div`
  color: ${({ theme }) => theme.text_primary};
  font-size: 24px;
  font-weight: 540;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const FavouriteContainer = styled.div`
display: flex;
flex-wrap: wrap;
gap: 14px;
padding: 18px 6px;
@media (max-width: 550px){
  justify-content: center;
}
`

const Favourites = ({isLoggedIn}) => {

  const { favorites } = useFavorite();
  const navigate = useNavigate();

React.useEffect(() => {
  if (!isLoggedIn) {
      navigate('/login');
  }
}, [isLoggedIn, navigate]);

if (!isLoggedIn) {
  return null; // Render nothing if not logged in (redirect handled by useEffect)
}


  return (
    <Container>
        <Topic>
            Favourites
        </Topic>
        <FavouriteContainer>
            {favorites.map(podcast => (
          <PodcastCard key={podcast._id} item={podcast} />
        ))}
        </FavouriteContainer>
    </Container>
  )
}

export default Favourites