import React from 'react';
import PodcastCard from '../components/PodcastCard';
import styled from 'styled-components';
import Avatar from '@mui/material/Avatar';
import { Button } from '@mui/material';
import { useFavorite } from '../components/FavoriteContext'; 
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const UserDetails = styled.div`
  display: flex;
  gap: 120px;
  align-items: center;
  @media (max-width: 768px) {
    width: fit-content;
    flex-direction: column; 
    gap: 20px;
    justify-content: center;
    align-items: center;
  }
`;
const ProfileContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (max-width: 768px) {
    align-items: center;
  }
`;
const ProfileAvatar = styled.div`
  padding-left: 3rem;
  @media (max-width: 768px) {
    padding-left: 0rem;
  }
`;
const ProfileName = styled.div`
  color: ${({ theme }) => theme.text_primary};
  font-size: 34px;
  font-weight: 500;
`;
const ProfileEmail = styled.div`
  color: #2b6fc2;
  font-size: 18px;
  font-weight: 400;
`;
const ProfileMain = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  @media (max-width: 768px) {
    align-items: center;
  }
`;
const FilterContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  ${({ box, theme }) => box && `
    background-color: ${theme.bg};
    border-radius: 10px;
    padding: 20px 30px;
  `}
`;
const Topic = styled.div`
  color: ${({ theme }) => theme.text_primary};
  font-size: 24px;
  font-weight: 540;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin : 20px;
`;
const Podcasts = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 14px;
  padding: 18px 6px;
  @media (max-width: 550px){
    justify-content: center;
  }
`;
const ActionButtons = styled.div`
  display: flex;
  gap: 10px;
  margin-top: 10px;
`;

const Profile = () => {
  const navigate = useNavigate()
  const userName = localStorage.getItem('userName') || 'User';
  const userEmail = localStorage.getItem('userEmail') || 'user@example.com';

  const { favorites } = useFavorite(); // Retrieve favorite podcasts using your context hook

  const handleEditProfile = () => {
    navigate('/edit-profile');
  };


  const handleDeleteAccount = async (email) => {
    try {
      const response = await axios.delete('http://localhost:5000/delete',{ data: { email: userEmail } });
      if (response.status === 200) {
        alert('Account deleted successfully!');
        localStorage.clear();
        navigate('/signup');
      }
    } catch (error) {
      console.error("Error deleting account:", error);
      alert('Failed to delete account. Please try again.');
    }
  };

  return (
    <ProfileMain>
      <UserDetails>
        <ProfileAvatar>
          <Avatar sx={{ height: 130, width: 130, fontSize: '50px' }}>
            {userName.charAt(0).toUpperCase()}
          </Avatar>
        </ProfileAvatar>
        <ProfileContainer>
          <ProfileName>{userName}</ProfileName>
          <ProfileEmail>{userEmail}</ProfileEmail>
          <ActionButtons>
            <Button variant="contained" color="primary" onClick={handleEditProfile}>
              Edit Profile
            </Button>
            <Button variant="contained" color="secondary" onClick={handleDeleteAccount}>
              Delete Account
            </Button>
          </ActionButtons>
        </ProfileContainer>
      </UserDetails>
      <FilterContainer>
        <Topic>Your Favourites</Topic>
        <Podcasts>
          {favorites.map((podcast) => (
            <PodcastCard key={podcast._id} item={podcast} />
          ))}
        </Podcasts>
      </FilterContainer>
    </ProfileMain>
  );
};

export default Profile;
