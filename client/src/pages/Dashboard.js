import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components';
import PodcastCard from '../components/PodcastCard';

const DashboardMain = styled.div`
    padding: 20px 30px;
    padding-bottom: 200px;
    height: 100%;
    overflow-y: scroll;
    display: flex;
    flex-direction: column;
    gap: 20px;
    @media (max-width: 768px){
    padding: 6px 10px;
    }
`;
const FilterContainer = styled.div`
    display: flex;
    flex-direction: column;
    ${({ box, theme }) => box && `
    background-color: ${theme.bg};
    border-radius: 10px;
    padding: 20px 30px;
    `}
    background-color: ${({ theme }) => theme.bg};
    border-radius: 10px;
    padding: 20px 30px;
`;
const Topic = styled.div`
    color: ${({ theme }) => theme.text_primary};
    font-size: 24px;
    font-weight: 540;
    display: flex;
    justify-content: space-between;
    align-items: center;
    @maedia (max-width: 768px){
        font-size: 18px;
    }
`;
const Span = styled.span`
    color: ${({ theme }) => theme.text_secondary};
    font-size: 16px;
    font-weight: 400;
    cursor: pointer;
    @media (max-width: 768px){
        font-size: 14px;
    }
    color: ${({ theme }) => theme.primary};
    &:hover{
        transition: 0.2s ease-in-out;
    }
  `;
const Podcasts = styled.div`
    display: flex;
    flex-wrap: wrap;
    gap: 14px;
    padding: 18px 6px;
    //center the items if only one item present
    @media (max-width: 550px){
    justify-content: center;
    }
`;

const Dashboard = () => {

    const [podcasts, setPodcasts] = useState([]);
    
    useEffect(() => {
        fetch('http://127.0.0.1:2900/podcast')
            .then(res => res.json())
            .then(data => {
                setPodcasts(data);
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
    }, []);


    return (
        <DashboardMain>
            <FilterContainer>
                <Topic>
                    Most Popular
                    <Link to={`/showpodcasts/mostpopular`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "mostpopular").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    Comedy
                    <Link to={`/showpodcasts/comedy`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                    {podcasts.filter(item => item.gener === "comedy").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    Music
                    <Link to={`/showpodcasts/music`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "music").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                Health and Wellness
                    <Link to={`/showpodcasts/health`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "health").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    Food
                    <Link to={`/showpodcasts/food`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "food").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    History
                    <Link to={`/showpodcasts/history`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "history").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    Crime
                    <Link to={`/showpodcasts/crime`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "crime").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    Society
                    <Link to={`/showpodcasts/society`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "society").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
            <FilterContainer>
                <Topic>
                    Science
                    <Link to={`/showpodcasts/science`} style={{ textDecoration: "none" }}>
                        <Span>Show All</Span>
                    </Link>
                </Topic>
                <Podcasts>
                {podcasts.filter(item => item.gener === "science").map((item) => (
                        <PodcastCard key={item._id} item = {item}/>
                    ))}
                </Podcasts>
            </FilterContainer>
            
        </DashboardMain>
    )
}

export default Dashboard