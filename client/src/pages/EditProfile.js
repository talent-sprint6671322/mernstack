// import React, { useState } from 'react';
// import styled from 'styled-components';
// import { Button, TextField, Avatar } from '@mui/material';
// import { useNavigate } from 'react-router-dom';

// const EditProfileContainer = styled.div`
//   display: flex;
//   flex-direction: column;
//   align-items: center;
//   padding: 20px;
//   background-color: ${({ theme }) => theme.bg};
//   border-radius: 16px;
//   max-width: 600px;
//   margin: auto;
// `;

// const AvatarContainer = styled.div`
//   margin-bottom: 20px;
// `;

// const FormContainer = styled.form`
//   display: flex;
//   flex-direction: column;
//   gap: 20px;
//   width: 100%;
// `;

// const FormControl = styled.div`
//   display: flex;
//   flex-direction: column;
//   gap: 8px;
// `;

// const ActionButtons = styled.div`
//   display: flex;
//   gap: 10px;
//   justify-content: flex-end;
// `;

// const EditProfile = () => {
//   const navigate = useNavigate();
//   const [userName, setUserName] = useState(localStorage.getItem('userName') || '');
//   const [userEmail, setUserEmail] = useState(localStorage.getItem('userEmail') || '');
//   const [password, setPassword] = useState('');
//   const [confirmPassword, setConfirmPassword] = useState('');

//   const handleSaveChanges = (e) => {
//     e.preventDefault();
//     // Implement save changes logic, such as API calls
//     // Update local storage as needed
//     localStorage.setItem('userName', userName);
//     localStorage.setItem('userEmail', userEmail);

//     // Navigate back to profile page or show success message
//     navigate('/profile');
//   };

//   const handleCancel = () => {
//     navigate('/profile');
//   };

//   return (
//     <EditProfileContainer>
//       <AvatarContainer>
//         <Avatar sx={{ height: 100, width: 100, fontSize: '40px' }}>
//           {userName.charAt(0).toUpperCase()}
//         </Avatar>
//       </AvatarContainer>
//       <FormContainer onSubmit={handleSaveChanges}>
//         <FormControl>
//           <TextField
//             label="Username"
//             value={userName}
//             onChange={(e) => setUserName(e.target.value)}
//             variant="outlined"
//             fullWidth
//           />
//         </FormControl>
//         <FormControl>
//           <TextField
//             label="Email"
//             type="email"
//             value={userEmail}
//             onChange={(e) => setUserEmail(e.target.value)}
//             variant="outlined"
//             fullWidth
//           />
//         </FormControl>
//         <FormControl>
//           <TextField
//             label="Password"
//             type="password"
//             value={password}
//             onChange={(e) => setPassword(e.target.value)}
//             variant="outlined"
//             fullWidth
//           />
//         </FormControl>
//         <FormControl>
//           <TextField
//             label="Confirm Password"
//             type="password"
//             value={confirmPassword}
//             onChange={(e) => setConfirmPassword(e.target.value)}
//             variant="outlined"
//             fullWidth
//           />
//         </FormControl>
//         <ActionButtons>
//           <Button variant="contained" color="primary" type="submit">
//             Save Changes
//           </Button>
//           <Button variant="outlined" color="secondary" onClick={handleCancel}>
//             Cancel
//           </Button>
//         </ActionButtons>
//       </FormContainer>
//     </EditProfileContainer>
//   );
// };

// export default EditProfile;



import React, { useState } from 'react';
import styled from 'styled-components';
import { Button, TextField, Avatar } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const EditProfileContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  background-color: ${({ theme }) => theme.bg};
  border-radius: 16px;
  max-width: 600px;
  margin: auto;
  color: ${({ theme }) => theme.text_primary};
`;

const AvatarContainer = styled.div`
  margin-bottom: 20px;
`;

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  gap: 20px;
  width: 100%;
`;

const FormControl = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
  
`;

const ActionButtons = styled.div`
  display: flex;
  gap: 10px;
  justify-content: flex-end;
`;
const StyledTextField = styled(TextField)`
  & .MuiInputBase-root.Mui-disabled {
    color: #cccccc !important;
    -webkit-text-fill-color: #cccccc !important; // Override for WebKit browsers
  }
  & .MuiInputLabel-root.Mui-disabled {
    color: #cccccc !important;
  }
`;


const EditProfile = () => {
  const navigate = useNavigate();
  const [userName, setUserName] = useState(localStorage.getItem('userName') || '');
  const [userEmail] = useState(localStorage.getItem('userEmail') || ''); // Do not allow editing
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleSaveChanges = async (e) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      alert('Passwords do not match');
      return;
    }

    try {
      const response = await axios.put('http://localhost:5000/update', {
        email: userEmail,
        userName,
        password,
      });

      if (response.status === 200) {
        // Update local storage
        localStorage.setItem('userName', userName);

        // Navigate back to profile page or show success message
        navigate('/profile');
      }
    } catch (error) {
      console.error('Error updating profile:', error);
      alert('Failed to update profile. Please try again.');
    }
  };

  const handleCancel = () => {
    navigate('/profile');
  };

  return (
    <EditProfileContainer>
      <AvatarContainer>
        <Avatar sx={{ height: 100, width: 100, fontSize: '40px' }}>
          {userName.charAt(0).toUpperCase()}
        </Avatar>
      </AvatarContainer>
      <FormContainer onSubmit={handleSaveChanges}>
        <FormControl>
          <TextField
            label="Username"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
            variant="outlined"
            fullWidth
            InputLabelProps={{ style: { color: 'inherit' } }}
            InputProps={{ style: { color: 'inherit' } }}
          />
        </FormControl>
        <FormControl>
          <TextField
            label="Email"
            type="email"
            value={userEmail}
            variant="outlined"
            fullWidth
            disabled // Disable email input field
            // InputLabelProps={{ style: { color: 'inherit' } }}
            // InputProps={{ style: { color: 'inherit' } }}
  
          />
        </FormControl>
        <FormControl>
          <TextField
            label="Password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            variant="outlined"
            fullWidth
            InputLabelProps={{ style: { color: 'inherit' } }}
            InputProps={{ style: { color: 'inherit' } }}
          />
        </FormControl>
        <FormControl>
          <TextField
            label="Confirm Password"
            type="password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            variant="outlined"
            fullWidth
            InputLabelProps={{ style: { color: 'inherit' } }}
            InputProps={{ style: { color: 'inherit' } }}
          />
        </FormControl>
        <ActionButtons>
          <Button variant="contained" color="primary" type="submit">
            Save Changes
          </Button>
          <Button variant="outlined" color="secondary" onClick={handleCancel}>
            Cancel
          </Button>
        </ActionButtons>
      </FormContainer>
    </EditProfileContainer>
  );
};

export default EditProfile;
