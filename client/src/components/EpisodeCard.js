
import React from 'react'
import styled from 'styled-components'
import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline';

const Card = styled.div`
    display: flex;
    flex-direction: row;
    gap: 20px;
    align-items: center;
    padding: 20px 30px;
    border-radius: 6px;
    background-color: ${({ theme }) => theme.card};
    cursor: pointer;
    &:hover{
        cursor: pointer;
        transform: translateY(-8px);
        transition: all 0.4s ease-in-out;
        box-shadow: 0 0 18px 0 rgba(0, 0, 0, 0.3);
        filter: brightness(1.3);
    }
    @media (max-width: 768px){
        flex-direction: column; 
        align-items: flex-start;
      }
`;

const Image = styled.img`
    width: 100px;
    height: 100px;
    border-radius: 6px;
    background-color: ${({ theme }) => theme.text_secondary};  
    object-fit: cover;
`;

const Details = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    width: 100%;
`;

const Title = styled.div`
    font-size: 18px;
    font-weight: 800;
    color: ${({ theme }) => theme.text_primary};
    width: 100%;
    display: flex;
    justify-content: space-between;
`;

const Description = styled.div`
    font-size: 14px;
    font-weight: 500;
    color: ${({ theme }) => theme.text_secondary};
`;
const ImageContainer = styled.div`
    position: relative;
    width: 100px;
    height: 100px;
`;


const Episodecard = ({ episode }) => {

    return (
        <Card >
            <ImageContainer>
                <Image src={episode.image} />
                <PlayCircleOutlineIcon style={{position:"absolute",top:"26px",left:"26px",color:"white",width:"50px",height:"50px"}}/>
            </ImageContainer>
            <Details>
                <Title>{episode.title}</Title>
                <Description>{episode.description}</Description>
            </Details>
        </Card>
    )
}

export default Episodecard







// import React, { useState, useRef } from 'react';
// import styled from 'styled-components';
// import PlayCircleOutlineIcon from '@mui/icons-material/PlayCircleOutline';

// const Card = styled.div`
//     display: flex;
//     flex-direction: row;
//     gap: 20px;
//     align-items: center;
//     padding: 20px 30px;
//     border-radius: 6px;
//     background-color: ${({ theme }) => theme.card};
//     cursor: pointer;
//     &:hover {
//         cursor: pointer;
//         transform: translateY(-8px);
//         transition: all 0.4s ease-in-out;
//         box-shadow: 0 0 18px 0 rgba(0, 0, 0, 0.3);
//         filter: brightness(1.3);
//     }
//     @media (max-width: 768px){
//         flex-direction: column; 
//         align-items: flex-start;
//     }
// `;

// const Image = styled.img`
//     width: 100px;
//     height: 100px;
//     border-radius: 6px;
//     background-color: ${({ theme }) => theme.text_secondary};  
//     object-fit: cover;
// `;

// const Details = styled.div`
//     display: flex;
//     flex-direction: column;
//     gap: 10px;
//     width: 100%;
// `;

// const Title = styled.div`
//     font-size: 18px;
//     font-weight: 800;
//     color: ${({ theme }) => theme.text_primary};
//     width: 100%;
//     display: flex;
//     justify-content: space-between;
// `;

// const Description = styled.div`
//     font-size: 14px;
//     font-weight: 500;
//     color: ${({ theme }) => theme.text_secondary};
// `;

// const ImageContainer = styled.div`
//     position: relative;
//     width: 100px;
//     height: 100px;
// `;

// const Episodecard = ({ episode }) => {
//     const [isPlaying, setIsPlaying] = useState(false);
//     const audioRef = useRef(new Audio(episode.audio)); // Create an audio element with the episode's audio URL

//     const togglePlay = () => {
//         if (isPlaying) {
//             audioRef.current.pause();
//         } else {
//             audioRef.current.play();
//         }
//         setIsPlaying(!isPlaying);
//     };

//     return (
//         <Card onClick={togglePlay}>
//             <ImageContainer>
//                 <Image src={episode.image} />
//                 <PlayCircleOutlineIcon
//                     style={{ position: "absolute", top: "26px", left: "26px", color: "white", width: "50px", height: "50px" }}
//                 />
//             </ImageContainer>
//             <Details>
//                 <Title>{episode.title}</Title>
//                 <Description>{episode.description}</Description>
//             </Details>
//             {/* Hidden audio element for audio playback */}
//             <audio ref={audioRef} src={episode.audio} />
//         </Card>
//     );
// };

// export default Episodecard;
