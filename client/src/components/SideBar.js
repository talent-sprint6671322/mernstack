import React from 'react'

import '../styles/SideBar.css'
import LogoImage from '../images/Logo.png'

import { IoMdHome } from "react-icons/io";
import { IoIosSearch } from "react-icons/io";
import { MdFavorite } from "react-icons/md";
import { IoMdClose } from "react-icons/io";
import { FaCloudUploadAlt } from "react-icons/fa";
import { MdLightMode } from "react-icons/md";
import { MdDarkMode } from "react-icons/md";
import { LuLogIn, LuLogOut } from "react-icons/lu";
import styled from 'styled-components';
import { Link } from 'react-router-dom';

// import HomeRoundedIcon from '@mui/icons-material/HomeRounded';

const MenuContainer = styled.div`
background-color: ${({ theme }) => theme.bg};
color: ${({ theme }) => theme.text_primary};
@media (max-width: 1100px) {
  position: fixed;
  z-index: 1000;
  width: 100%;
  max-width: 250px;
  left: ${({ menuOpen }) => (menuOpen ? "0" : "-100%")};
  transition: 0.3s ease-in-out;
}
`;
const Logo = styled.div`
color: ${({ theme }) => theme.primary};
`;
const Elements = styled.div` 
color:  ${({ theme }) => theme.text_secondary};
&:hover{
    background-color: ${({ theme }) => theme.text_secondary + 50};
}
`;
const Hr = styled.div`
width: 100%;
height: 1px;
background-color: ${({ theme }) => theme.text_secondary + 50};
margin: 10px 0px;
`;



export const SideBar = ({ menuOpen, setMenuOpen, darkMode, setDarkMode, isLoggedIn,handleLogout }) => {

    const handleTheme = () => {
        setDarkMode(!darkMode)
    }

    return (
        <MenuContainer className='MenuContainer' menuOpen={menuOpen}>
            <Link style={{ textDecoration: "none", color: "inherit" }}>
                <div className='Flex'>
                    <Logo className='logo'>
                        <img src={LogoImage} height={"30px"} />
                        Podstream
                    </Logo>
                    <div className='closeIcon' onClick={() => { setMenuOpen(!menuOpen) }}> <IoMdClose /></div>
                </div>
            </Link>
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                <Elements className='elements'>
                    <div className='icons'><IoMdHome /></div>
                    {/* <HomeRoundedIcon /> */}
                    <div className='navText'>Dashboard</div>
                </Elements>
            </Link>
            <Link to="/search" style={{ textDecoration: "none", color: "inherit" }}>
                <Elements className='elements'>
                    <div className='icons'><IoIosSearch /></div>
                    <div className='navText'>Search</div>
                </Elements>
            </Link>
            <Link to="/favorites" style={{ textDecoration: "none", color: "inherit" }}>
                <Elements className='elements'>
                    <div className='icons'><MdFavorite /></div>
                    <div className='navText'>Favourites</div>
                </Elements>
            </Link>
            <Hr />
            {/* <Elements className='elements' onClick={() => { console.log("UPLOAD"); }}>
                <div className='icons'><FaCloudUploadAlt /></div>
                <div className='navText'>Upload</div>
            </Elements> */}
            <Elements className='elements' onClick={handleTheme}>
                <div className='icons'>{darkMode ? <MdLightMode /> : <MdDarkMode />}</div>
                <div className='navText'>{darkMode ? "Light Mode" : "Dark Mode"}</div>
            </Elements>
            {isLoggedIn ? (
                <Elements className='elements' onClick={handleLogout}>
                    <div className='icons'><LuLogOut /></div>
                    <div className='navText'>Logout</div>
                </Elements>
            ) : (
                <Link to="/login" style={{ textDecoration: "none", color: "inherit" }}>
                    <Elements className='elements'>
                        <div className='icons'><LuLogIn /></div>
                        <div className='navText'>Login</div>
                    </Elements>
                </Link>
            )}


        </MenuContainer>
    )
}
