import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { FaUser } from "react-icons/fa";
import styled from 'styled-components';
import MenuIcon from "@mui/icons-material/Menu";
import { IconButton } from "@mui/material";
import Avatar from '@mui/material/Avatar';

const Navbar = styled.div`
display : flex;
justify-content : space-between;
padding : 15px 40px;
align-items:center;
box-sizing: border-box;
color : ${({ theme }) => theme.text_primary};
gap: 30px;
background: ${({ theme }) => theme.bg} !important
border-radius: 16px;
@media (max-width: 768px) {
    padding: 16px;
  }
`;
const Buttondiv = styled.div`
font-size: 14px;
  cursor: pointer;
  text-decoration: none;
  color: ${({ theme }) => theme.primary};
  border: 1px solid ${({ theme }) => theme.primary};
  border-radius: 12px;
  width: 100%;
  max-width: 70px;
  padding: 8px 10px;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 6px;
  &:hover{
    background-color: ${({ theme }) => theme.primary};
    color: ${({ theme }) => theme.text_primary};
  }
`;
const IcoButton = styled(IconButton)`
  color: ${({ theme }) => theme.text_secondary} !important;
`;
const AvatarContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 8px;
  cursor: pointer;
`;

const ProfileDropdown = styled.div`
  position: absolute;
  top: 50px;
  right: 0;
  // background: white;
  background-color: ${({ theme }) => theme.text_secondary};
  padding: 8px;
  border: 1px solid ${({ theme }) => theme.text_secondary};
  border-radius: 8px;
  z-index: 999;
`;

const ProfileDropdownItem = styled.div`
  padding: 8px;
  cursor: pointer;
  color: ${({ theme }) => theme.bgLight};
  &:hover {
    background-color: ${({ theme }) => theme.text_secondary + '50'};
    color: ${({ theme }) => theme.text_primary};
  }
`;

const NavBar = ({ menuOpen, setMenuOpen, isLoggedIn, setIsLoggedIn }) => {

  const navigate = useNavigate();
  const [profileDropdownOpen, setProfileDropdownOpen] = useState(false);
  const userEmail = localStorage.getItem('userEmail') || 'user@gmail.com';
  const userName = localStorage.getItem('userName') || 'User';

  const handleProfileDropdownClick = () => {
    setProfileDropdownOpen(!profileDropdownOpen);
  };

  const handleLogout = () => {
    localStorage.removeItem('userToken'); 
    setIsLoggedIn(false); 
    setProfileDropdownOpen(false); 
    navigate('/login');
  };

  const handleLoginClick = () => {
    navigate('/login');
  };

  const handleProfileClick = () => {
    setProfileDropdownOpen(false);
    navigate('/profile');
  };

  return (
    <Navbar >
      <IcoButton onClick={() => setMenuOpen(!menuOpen)}>
        <MenuIcon />
      </IcoButton>
      {isLoggedIn ? (
        <>
          <div style={{fontSize:'20px'}}>Welcome <span style={{color:'green',fontWeight:'bold'}}>{userName}</span></div>
          <AvatarContainer onClick={handleProfileDropdownClick}>
            {userEmail}
            <Avatar src="avatar.jpg" alt="User Avatar" >{userName.charAt(0).toUpperCase()}</Avatar>

          </AvatarContainer>
          {profileDropdownOpen && (
            <ProfileDropdown>
              <ProfileDropdownItem onClick={handleProfileClick}>Profile</ProfileDropdownItem>
              <ProfileDropdownItem onClick={handleLogout}>Logout</ProfileDropdownItem>
            </ProfileDropdown>
          )}
        </>
      ) : (
        <Buttondiv onClick={handleLoginClick}>
          <FaUser /> Login
        </Buttondiv>
      )}
    </Navbar>
  )
}

export default NavBar

