import React, { useState, useEffect } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';
import styled from 'styled-components';

const url = 'http://localhost:5000/register';
const getUsersUrl = 'http://localhost:5000/fetch';

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background: ${({ theme }) => theme.bgLight};
`;

const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${({ theme }) => theme.bgDark};
  padding: 40px;
  border-radius: 8px;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
  width: 100%;
  max-width: 400px;
`;

const Title = styled.h1`
  color: ${({ theme }) => theme.text_primary};
  margin-bottom: 20px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Input = styled.input`
  padding: 10px;
  margin-bottom: 20px;
  border-radius: 4px;
  border: 1px solid ${({ theme }) => theme.text_secondary};
  outline: none;
  font-size: 16px;
  color: ${({ theme }) => theme.text_primary};
  background: ${({ theme }) => theme.bgLight};
`;

const Button = styled.button`
  padding: 10px;
  border-radius: 4px;
  border: none;
  cursor: pointer;
  background: ${({ theme }) => theme.text_primary};
  color: ${({ theme }) => theme.bgLight};
  font-size: 16px;
  margin-bottom: 10px;
  &:hover {
    background: ${({ theme }) => theme.text_secondary};
  }
`;

const ErrorText = styled.p`
  color: red;
  margin-bottom: 20px;
`;

const SignupLink = styled.p`
  margin-top: 20px;
  color: ${({ theme }) => theme.text_secondary};
  a {
    color: ${({ theme }) => theme.text_primary};
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`;


function SignUp() {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [email, setEmail] = useState('');
    const [usersLength, setUsersLength] = useState(0);
    const [error, setError] = useState('');

    useEffect(() => {
        const fetchUsersLength = async () => {
            try {
                const response = await axios.get(getUsersUrl);
                setUsersLength(response.data.length);
            } catch (error) {
                console.error('Error fetching users:', error);
            }
        };

        fetchUsersLength();
    }, []);

    

    const navigate = useNavigate();

    const handleIncrease = async (e) => {
        e.preventDefault();
        if (password !== confirmPassword) {
            setError('Passwords do not match');
            return;
        }
        const newUser = { id: usersLength, name, password, email };
        try {
            const response = await axios.post(url, newUser);
            console.log(response.data);
            handleReset();
            navigate('/login');
        } catch (error) {
            console.error('Error registering new user:', error);
            if (error.response && error.response.status === 400) {
                setError('Email already exists');
            } else {
                setError('An error occurred while registering. Please try again later.');
            }
        }
    };

    const handleReset = () => {
        setName('');
        setEmail('');
        setPassword('');
        setConfirmPassword('');
        setError('');
    };

    return (
        <Container className="reg-container">
            <FormContainer className="reg-form">
                <Title>Sign Up</Title>
                <Form onSubmit={handleIncrease} onReset={handleReset}>
                    <Input
                        type="text"
                        placeholder="Name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        required
                    />
                    <Input
                        type="email"
                        placeholder="E-mail"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                    <Input
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                    <Input
                        type="password"
                        placeholder="Confirm Password"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        required
                    />
                    {error && <ErrorText style={{ color: 'red' }}>{error}</ErrorText>}
                    <Button type="submit">Signup</Button>
                    <Button type="reset">Reset</Button>
                </Form>
                <SignupLink className="signuplink">
                    Already have an account? <NavLink to="/login" style={{color:'red'}}>Login here</NavLink>.
                </SignupLink>
            </FormContainer>
        </Container>
    );
}

export default SignUp;