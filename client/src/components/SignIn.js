import React, { useRef, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import axios from 'axios';
import styled from 'styled-components';
// import { useAuth } from './AuthContext';


const loginUrl = 'http://localhost:5000/login';

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background: ${({ theme }) => theme.bgLight};
`;

const LoginForm = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${({ theme }) => theme.bgDark};
  padding: 40px;
  border-radius: 8px;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.5);
  width: 100%;
  max-width: 400px;
`;

const Title = styled.h1`
  color: ${({ theme }) => theme.text_primary};
  margin-bottom: 20px;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const Input = styled.input`
  padding: 10px;
  margin-bottom: 20px;
  border-radius: 4px;
  border: 1px solid ${({ theme }) => theme.text_secondary};
  outline: none;
  font-size: 16px;
  color: ${({ theme }) => theme.text_primary};
  background: ${({ theme }) => theme.bgLight};
`;

const Button = styled.button`
  padding: 10px;
  border-radius: 4px;
  border: none;
  cursor: pointer;
  background: ${({ theme }) => theme.text_primary};
  color: ${({ theme }) => theme.bgLight};
  font-size: 16px;
  &:hover {
    background: ${({ theme }) => theme.text_secondary};
  }
`;

const ErrorText = styled.p`
  color: red;
  margin-bottom: 20px;
`;

const SignupLink = styled.p`
  margin-top: 20px;
  color: ${({ theme }) => theme.text_secondary};
  a {
    color: ${({ theme }) => theme.text_primary};
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`;

function SignIn({ setIsLoggedIn }) {
    const emailRef = useRef(null);
    const pwdRef = useRef(null);
    const navigate = useNavigate();
    // const { login } = useAuth();
    const [error, setError] = useState('');

    const handleLoginSuccess = (userData) => {
        localStorage.setItem('userEmail', userData.user.email);
        localStorage.setItem('userName', userData.user.name);
        setIsLoggedIn(true);
      };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const email = emailRef.current.value;
        const password = pwdRef.current.value;

        try {
            const response = await axios.post(loginUrl, { email, password, login: true });
            console.log(response);

            if (response.status === 200) {
                setError('');
                setIsLoggedIn(true);
                navigate('/');
                const userData = response.data;
                console.log(userData);
                handleLoginSuccess(userData)
            } 
            else if (response.status === 401) {
                if (response.data.message === 'Invalid password') {
                    setError('Incorrect password. Please try again.');
                } else if (response.data.message === 'User not found') {
                    setError('User not found. Please check your email.');
                } else {
                    setError('Invalid credentials. Please try again.');
                }
            }

            // else{
            //     const errorMessage = response.data.message || 'An error occurred during authentication.';
            //     setError(errorMessage);
            // }
            // else if (response.status === 401) {
            //     setError('Incorrect email or password.');
            // }
            // else {
            //     setError('Invalid credentials. Please try again.');
            // }
        } catch (error) {
            console.error('Error during login:', error);
            console.log('Error response:', error.response);
            setError('An error occurred while authenticating. Please try again later.');
        }
    };


    return (
        <Container className='con'>
            <LoginForm className='log'>
                <Title>Login</Title>
                <Form onSubmit={handleSubmit}>
                    <Input
                        type="text"
                        placeholder="Email"
                        required
                        ref={emailRef}
                    />
                    <Input
                        type="password"
                        placeholder="Password"
                        required
                        ref={pwdRef}
                    />
                    <br /><br />
                    {error && <ErrorText style={{ color: 'red' }}>{error}</ErrorText>}
                    <Button type='submit'>Login</Button>
                </Form>
                <SignupLink className='signuplink'>If you're a new user <NavLink to='/signup' style={{color:'red'}}>signup</NavLink> here</SignupLink><br />
            </LoginForm>
        </Container>
    );
}

export default SignIn;


