
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import styled, { ThemeProvider } from "styled-components";
import { darkTheme, lightTheme } from './utils/Themes'
import { useState } from 'react';

import { SideBar } from "./components/SideBar";
import NavBar from './components/NavBar';
import Dashboard from './pages/Dashboard';
import Search from './pages/Search';
import Favourites from './pages/Favourites';
import Profile from './pages/Profile';
import PodcastDetails from './pages/PodcastDetails';
import DisplayPodcasts from './pages/DisplayPodcasts';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import { FavoriteProvider } from './components/FavoriteContext';
import EditProfile from './pages/EditProfile';

const Frame = styled.div`
  display: flex;
  flex-direction: column;
  flex: 3;

`;
const Container = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    height: 100vh;
    background: ${({ theme }) => theme.bgLight};
    overflow-y: hidden;
    overflow-x: hidden;
  `;



function App() {

  const [darkMode, setDarkMode] = useState(true);
  const [menuOpen, setMenuOpen] = useState(true);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
  };

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <BrowserRouter>
      <FavoriteProvider>
        <Container className="container" >
          {menuOpen && (
            <SideBar
              menuOpen={menuOpen}
              setMenuOpen={setMenuOpen}
              darkMode={darkMode}
              setDarkMode={setDarkMode}
              isLoggedIn={isLoggedIn}
              handleLogout={() => setIsLoggedIn(false)}
            />
          )}
          <Frame>
            <NavBar menuOpen={menuOpen}
              setMenuOpen={setMenuOpen}
              setIsLoggedIn={setIsLoggedIn}
              isLoggedIn={isLoggedIn}
            />
            
            <Routes>
              <Route path='/' element={<Dashboard isLoggedIn={isLoggedIn}/>} />
              <Route path="/login" element={<SignIn setIsLoggedIn={setIsLoggedIn} />} />
              <Route path="/signup" element={<SignUp />} />
              <Route path='/search' exact element={<Search isLoggedIn={isLoggedIn} />} />
              <Route path='/favorites' exact element={<Favourites isLoggedIn={isLoggedIn} />} />
              <Route path='/profile' exact element={<Profile />} />
              <Route path="/edit-profile" element={<EditProfile />} />
              <Route path='/podcast/:id' element={<PodcastDetails />} />
              <Route path='/showpodcasts/:gener' element={<DisplayPodcasts />} />
            </Routes>

          </Frame>
        </Container>
        </FavoriteProvider>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
