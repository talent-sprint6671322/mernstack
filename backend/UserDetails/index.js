const express = require('express')
const app = express()
const cors = require('cors')

const port = 5000;

app.use(express.json())
app.use(cors())
app.use('/register',require('./routes/Register'))
app.use('/fetch',require('./routes/fetch'))
app.use('/update',require('./routes/update'))
app.use('/delete',require('./routes/delete'))
app.use('/login', require('./routes/Login'));

app.listen(port,(req,res)=>{
    console.log(`Server is running at ${port}`);
})