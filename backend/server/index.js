const express = require('express');
const cors = require('cors')
const app = express();
const port = 2900;
app.use(cors())

app.use(express.json())  // to convert our data to json format

app.use('/podcast',require('./routes/podcast'));
app.use('/',require('./routes/podcastBy_Id'))
app.use('/search',require('./routes/search'))
app.use('/podbygener',require('./routes/podcastByGener'))

app.listen(port,(req,res)=>{
    console.log(`Server is running at ${port}`);
})