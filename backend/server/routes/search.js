const express = require('express');
const router = express.Router();
const mongodb = require('mongodb').MongoClient;

module.exports = router.get('/',(req,res)=>{
    const searchTerm = req.query.title
    mongodb.connect('mongodb://localhost:27017/Podstream',(err,db)=>{
        if(err) throw err;
        else{
            
            db.collection('trending').find({ title: { $regex: searchTerm, $options: 'i' } }).toArray((err,data)=>{
                if(err){
                    console.error("While fetching data from Collection",err)
                }
                else{
                    if(data.length > 0){
                        res.status(200).send(data)
                    }
                    else{
                        res.status(404).send("User Not Found")
                    }
                }
            })
        }
    })
})
