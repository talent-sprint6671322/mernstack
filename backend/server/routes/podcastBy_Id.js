const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb'); // Import ObjectId from mongodb package

const mongo = require('mongodb').MongoClient;

module.exports = router.get('/podcastbyid/:id', (req, res) => {
    const id = req.params.id; // Get the id parameter from the request

    // Connect to MongoDB
    mongo.connect('mongodb://localhost:27017/Podstream', (err, db) => {
        if (err) {
            console.error("Error connecting to MongoDB:", err);
            res.status(500).send("Internal Server Error");
            return;
        }

        // Find the document by _id
        db.collection('trending').findOne({ _id: ObjectId(id) }, (err, record) => {
           
            if (err) {
                console.error("Error finding document:", err);
                res.status(500).send("Internal Server Error");
                return;
            }

            if (record) {
                res.status(200).json(record); // Document found, send it in the response
                console.log(record);
            } else {
                res.status(404).send("Document Not Found"); // Document not found
            }
        });
    });
});